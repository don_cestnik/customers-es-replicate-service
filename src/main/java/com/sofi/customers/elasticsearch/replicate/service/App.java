package com.sofi.customers.elasticsearch.replicate.service;

import com.sofi.avro.schemas.Customer;
import com.sofi.avro.schemas.CustomerAddress;
import com.sofi.elasticsearch.client.client.ElasticSearchRESTClient;

import io.confluent.kafka.serializers.KafkaAvroDecoder;
import org.apache.avro.specific.SpecificData;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String... args) {
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("metadata.broker.list", "localhost:29092");
        kafkaParams.put("schema.registry.url", "http://localhost:28081");
        JavaStreamingContext ssc = new JavaStreamingContext(new JavaSparkContext(new SparkConf().setAppName("kafka-sandbox").setMaster("local[*]")), Durations.seconds(10));

        ElasticSearchRESTClient client = new ElasticSearchRESTClient("http", "localhost", "9200");

        KafkaUtils.createDirectStream(ssc, Object.class, Object.class, KafkaAvroDecoder.class, KafkaAvroDecoder.class, kafkaParams, Collections.singleton("com.sofi.CustomerChanged")).foreachRDD(rdd -> {
            rdd.foreach(i -> {
                Customer c = (Customer) SpecificData.get().deepCopy(Customer.getClassSchema(), i._2);
                System.out.println(c);
                client.put(c.getId(), c, "customers", "customer");
            });
        });


        KafkaUtils.createDirectStream(ssc, Object.class, Object.class, KafkaAvroDecoder.class, KafkaAvroDecoder.class, kafkaParams, Collections.singleton("com.sofi.CustomerAddressChanged")).foreachRDD(rdd -> {
            rdd.foreach(i -> {
                CustomerAddress c = (CustomerAddress) SpecificData.get().deepCopy(CustomerAddress.getClassSchema(), i._2);
                System.out.println(c);
            });
        });

        ssc.start();
        ssc.awaitTermination();
    }
}
