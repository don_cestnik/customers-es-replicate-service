name := "customers-es-replicate-service"

version := "1.0-SNAPSHOT"

organization := "com.sofi"

resolvers ++= Seq("Confluent Maven Repo" at "http://packages.confluent.io/maven/")

libraryDependencies ++= Seq(
    "com.sofi"         % "avro-schemas"               % "1.0-SNAPSHOT" exclude("com.fasterxml.jackson.core", "jackson-databind"),
    "com.sofi"         % "es-client"                  % "1.0-SNAPSHOT",
    "com.sofi"         % "kafka-utils"                % "release-4" exclude("com.fasterxml.jackson.core", "jackson-databind"),
    "io.confluent"     % "kafka-avro-serializer"      % "3.1.1",
    "org.apache.spark" % "spark-streaming_2.11"       % "1.6.0",
    "org.apache.spark" % "spark-streaming-kafka_2.11" % "1.6.0"
)

libraryDependencies := libraryDependencies.value.map(_.exclude("org.slf4j", "slf4j-log4j12"))

dependencyOverrides ++= Set( 
"com.fasterxml.jackson.core" % "jackson-databind" % "2.4.4" 
)

lazy val testDependencies = Seq(
    "junit" % "junit" % "4.10"
)

libraryDependencies ++= testDependencies.map(_ % "test")

testOptions += Tests.Argument(TestFrameworks.JUnit, "-v", "-q")

EclipseKeys.projectFlavor := EclipseProjectFlavor.Java

autoScalaLibrary := false

crossPaths := false

publishMavenStyle := true
